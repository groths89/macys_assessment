const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: "development",
    entry: path.resolve(__dirname, './src/index.js'),
    devServer: {
        port: 8080,
        static: {
            directory: path.join(__dirname, 'public'),
          },
        historyApiFallback: {
            index: 'index.html'
        }
    },
    output: {
        filename: "main.js",
        publicPath: "/",
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
        }),        
    ],
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', { targets: "defaults" }]
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    "css-loader"
                ],
            }
        ]
    },
}