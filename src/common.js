import {Chart, registerables} from 'chart.js';
Chart.register(...registerables);

const jsonFeed = require('../test_feed.json');

function wordCount() {
    let totalOfWords = 0;
    let contentData = jsonFeed.content[0];
    let contentBodyHtml = contentData.content.bodyHtml;
    console.table(contentBodyHtml);
    let openingAngleBracket = "\u2039";
    let closingAngleBracket = "\u203a"
    let data = [];
      for (let index = 0; index < contentBodyHtml.length; index++) {
            if (contentBodyHtml[index] === " ") {
              totalOfWords += 1;
              data.push(totalOfWords);              
            }
      }
    console.table(data);
    return data;
}

  const data = {
    datasets: [{
      label: 'My First dataset',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      data: wordCount(),
    }]
  };

  const config = {
    type: 'radar',
    data: data,
    options: {
      elements: {
        line: {
          borderWidth: 3
        }
      }
    }
  };



const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, config);

